FROM node:6

COPY src/index.js src/index.js 
RUN cd /src && \
    npm install mongodb && \
    npm link mongodb

VOLUME /data

ENV MONGO_URL=mongodb://mongo:27017/unicorns

CMD ["node", "src/index.js"]