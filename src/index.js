var MongoClient = require('mongodb').MongoClient;

var url = process.env.MONGO_URL;

MongoClient.connect(url, function (err, db) {
  if (err) {
    console.log('Unable to connect ',err);
  } else {
    console.log('Connected');

    var collection = db.collection('unicorns');

    var un1 = {name: 'Marry', dob: new Date(1999,7,14,8,56), loves: ['me','you'], weight: 459, gender: 'f', vampires: 67};
    var un2 = {name: 'Sppedy', dob: new Date(1995,23,3,5,45), loves: ['krack'], weight: 900, gender: 'm', vampires: 56};

    collection.insert([un1, un2], function (err, res) {
      if (err) {
        console.log(err);
      } else {
        console.log(res);
      }
    //db.close();
  });
  }
});